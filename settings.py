#     Simple Bot - multifunctional bot
#     Copyright (C) 2021  Shandy developer agency
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import discord
from discord.ext import commands
from discord_slash import cog_ext

from basic_module import BasicModule


class Settings(BasicModule):
    @commands.group(invoke_without_command=True)
    async def debug(self, ctx):
        voice_id = self.cache.get_server(ctx.guild.id).voice_channel
        user = self.cache.get_user(ctx.author.id)
        user_preferences = str()
        for attr in dir(user):
            if not attr.startswith('_') and not callable(getattr(user, attr)):
                user_preferences += f"**{attr}**```{getattr(user, attr)} ```\n"
        embed = discord.Embed(title="Debug <:bug:822117306690961418>",
                              description="Эта информация полезна при разработке, но почему бы ее не посмотреть.",
                              colour=0x8691c8)
        embed.add_field(name="voice_id", value=voice_id, inline=False)
        embed.add_field(name="user_preferences", value=user_preferences, inline=False)
        await ctx.send(embed=embed)

    @debug.command()
    async def channels(self, ctx):
        embed = discord.Embed(title="Debug channels <:bug:822117306690961418>",
                              description="Эта информация полезна при разработке, но почему бы ее не посмотреть.",
                              colour=0x8691c8)
        embed.add_field(name="channels", value=self.cache.get_temp_channel(), inline=False)
        await ctx.send(embed=embed)

    @debug.command()
    async def guilds(self, ctx):
        embed = discord.Embed(title="Debug guilds <:bug:822117306690961418>",
                              description="Эта информация полезна при разработке, но почему бы ее не посмотреть.",
                              colour=0x8691c8)
        for guild in self.bot.guilds:
            embed.add_field(name=guild.name, value=(guild.id, guild.member_count, guild.owner.name))
        await ctx.send(embed=embed)

    @cog_ext.cog_subcommand(base="set", name="channel", description="Установить голосовой канал для создания временных", )
    async def channel(self, ctx, channel: discord.VoiceChannel):
        voice_channels = ctx.guild.voice_channels
        server_channel = self.bot.get_channel(channel.id)
        if server_channel not in voice_channels:
            embed = discord.Embed(title=":x: Ошибка", description="Такого канала не существует!", colour=0x8691c8)
            await ctx.send(embed=embed)
        else:
            self.cache.get_server(ctx.guild.id).change_voice_channel(channel.id)
            embed = discord.Embed(title=f":white_check_mark: Канал успешно обновлен!", colour=0x8691c8)
            await ctx.send(embed=embed)

