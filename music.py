#     Simple Bot - multifunctional bot
#     Copyright (C) 2021  Shandy developer agency
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import asyncio
import configparser

import discord
import youtube_dl
from discord.ext import commands

from basic_module import BasicModule

config = configparser.ConfigParser()
config.read("config.ini", encoding='utf-8')
# Suppress noise about console usage from errors
youtube_dl.utils.bug_reports_message = lambda: ''

ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0'  # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)


class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)

        self.data = data

        self.title = data.get('title')
        self.url = data.get('url')

    @classmethod
    async def from_url(cls, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))

        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]

        filename = data['url'] if stream else ytdl.prepare_filename(data)
        return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)


class Music(BasicModule):
    @commands.Cog.listener()
    async def on_voice_state_update(self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState):
        if before.channel is None:
            return
        if member.guild.get_member(self.bot.user.id).voice is None:
            return
        if (before.channel == member.guild.get_member(self.bot.user.id).voice.channel) and (
                (after.channel is None) or (after.channel != before.channel)) and len(
            member.guild.get_member(self.bot.user.id).voice.channel.members) == 1:
            await member.guild.change_voice_state(channel=None)

    @commands.command()
    async def join(self, ctx):
        """Joins a voice channel"""
        if not ctx.author.voice.channel:
            await self.send_error(ctx, "Вы не в канале")
            return

        channel = ctx.author.voice.channel

        if ctx.voice_client is not None:
            return await ctx.voice_client.move_to(channel)

        await ctx.guild.change_voice_state(channel=channel)

    @commands.command()
    async def play(self, ctx, *, url: str):
        radiostations = {
            "маруся": "http://radio-holding.ru:9000/marusya_default",
        }
        if url.startswith("https://youtube.com/watch?") or url.startswith("https://youtu.be/") or url.startswith(
                "https://www.youtube.com/watch?"):
            async with ctx.typing():
                player = await YTDLSource.from_url(url, loop=self.bot.loop, stream=True)
                ctx.voice_client.play(player, after=lambda e: print(f'Player error: {e}') if e else None)
                embed = discord.Embed(title="Сейчас играет", description=player.title)
            await ctx.send(embed=embed)
        elif url.lower() in radiostations:
            url_radio = radiostations[url]
            source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(url_radio))
            ctx.voice_client.play(source, after=lambda e: print(f'Player error: {e}') if e else None)
            embed = discord.Embed(title="Сейчас играет", description="Радиостанция: " + url)
            await ctx.send(embed=embed)
        else:
            source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(url))
            ctx.voice_client.play(source, after=lambda e: print(f'Player error: {e}') if e else None)
            embed = discord.Embed(title="Сейчас играет", description=url)
            await ctx.send(embed=embed)

    @commands.command()
    async def volume(self, ctx, volume: int):
        """Changes the player's volume"""

        if ctx.voice_client is None:
            return await self.send_error(ctx, "Не в канале")

        ctx.voice_client.source.volume = volume / 100
        await ctx.send(f"Changed volume to {volume}%")

    @commands.command()
    async def stop(self, ctx):
        """Stops and disconnects the bot from voice"""

        await ctx.voice_client.disconnect()

    @play.before_invoke
    async def ensure_voice(self, ctx):
        if ctx.voice_client is None:
            if ctx.author.voice:
                await ctx.author.voice.channel.connect()
            else:
                await self.send_error(ctx, "Вы не в канале")
                raise commands.CommandError("Author not connected to a voice channel.")
        elif ctx.voice_client.is_playing():
            ctx.voice_client.stop()
