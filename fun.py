#     Simple Bot - multifunctional bot
#     Copyright (C) 2021  Shandy developer agency
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import discord
from discord.ext import commands
from basic_module import BasicModule


class Fun(BasicModule):
    @commands.command()
    async def old(self, ctx):
        def sort_by_age(emp):
            return emp.created_at

        medals = {1: "🥇", 2: "🥈", 3: "🥉"}
        members: list[discord.Member] = ctx.guild.members
        members.sort(key=sort_by_age)
        embed = discord.Embed(title="Список старых пользователей Discord",
                              description="Отсортировано по дате регистрации")
        for i, m in enumerate(members, start=1):
            embed.add_field(name=f"{i}. {m.display_name} {medals.get(i, '')}",
                            value=f"<t:{int(m.created_at.timestamp())}:D>\n<t:{int(m.created_at.timestamp())}:R>")
        await ctx.send(embed=embed)
