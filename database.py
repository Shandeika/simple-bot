#     Simple Bot - multifunctional bot
#     Copyright (C) 2021  Shandy developer agency
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import pymysql


class Database:
    def __init__(self, host: str, user: str, database: str, password: str, port: int):
        self.database = database
        self.conn = pymysql.connect(
            host=host,
            user=user,
            password=password,
            database=database,
            port=port,
            autocommit=True,
            cursorclass=pymysql.cursors.DictCursor,
        )

    def execute(self, sql_query: str, args=None):
        """Execute an SQL query without receiving a response. For example, UPDATE or DELETE"""
        with self.conn.cursor() as cur:
            self.conn.ping(reconnect=True)
            cur.execute(sql_query, args)

    def fetch_one(self, sql_query: str, args=None):
        """Execute an SQL query with the return of a single element. For example, to request data about a user by id"""
        with self.conn.cursor() as cur:
            self.conn.ping(reconnect=True)
            cur.execute(sql_query, args)
            return cur.fetchone()

    def fetch_all(self, sql_query: str, args=None):
        """Execute an SQL query with the return of all elements. For example, it is suitable for displaying a list of all users"""
        with self.conn.cursor() as cur:
            self.conn.ping(reconnect=True)
            cur.execute(sql_query, args)
            return cur.fetchall()
