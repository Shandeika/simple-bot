#     Simple Bot - multifunctional bot
#     Copyright (C) 2021  Shandy developer agency
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import datetime
import sys

import discord
import psutil
from discord.ext import commands
from discord_slash import cog_ext, SlashContext

from basic_module import BasicModule


class Mod(BasicModule):
    def __init__(self, bot, cache, db, config):
        super().__init__(bot, cache, db, config)
        self.start_time = datetime.datetime.today()

    @cog_ext.cog_slash(name="status", description="Посмотреть информацию о состоянии бота")
    async def status(self, ctx: discord.ext.commands.Context):
        uptime_timedelta: datetime.timedelta = datetime.datetime.today() - self.start_time
        uptime = str(uptime_timedelta).split(".")[0]
        server_uptime = datetime.datetime.fromtimestamp(psutil.boot_time())
        embed = discord.Embed(title=f"Статус {self.bot.user.name}",
                              color=0x8691c8)
        embed.add_field(name="Версия Python", value=sys.version[:3])
        embed.add_field(name="Версия discord.py", value=discord.__version__)
        embed.add_field(name="Uptime", value=uptime)
        embed.add_field(name="Информация о сервере", value=f"Загрузка ЦП: `{psutil.cpu_percent(interval=None)}%`\n"
                                                           f"RAM: `{psutil.virtual_memory().used//1024//1024} МБ`/`{psutil.virtual_memory().total//1024//1024} МБ`\n"
                                                           f"Uptime: `{str(datetime.datetime.today() - server_uptime).split('.')[0]}`")
        embed.add_field(name="Временнных каналов", value=len(self.cache.get_temp_channel()))
        await ctx.send(embed=embed)

    @cog_ext.cog_slash(name="ServerInfo", description="Посмотреть информацию о сервере")
    async def serverinfo(self, ctx):
        bots, online, idle, dnd, offline = 0, 0, 0, 0, 0
        verif_level = {
            discord.VerificationLevel.none: "Отсутсвует",
            discord.VerificationLevel.low: "Низкий",
            discord.VerificationLevel.medium: "Средний",
            discord.VerificationLevel.high: "Высокий",
            discord.VerificationLevel.extreme: "Самый высокий"
        }
        for m in ctx.guild.members:  # проход по всем пользователям и сохранение количества
            if m.bot:
                bots += 1
                continue
            elif m.status == discord.Status.online:
                online += 1
            elif m.status == discord.Status.idle:
                idle += 1
            elif m.status == discord.Status.dnd:
                dnd += 1
            elif m.status == discord.Status.offline:
                offline += 1
        embed = discord.Embed(title=f"Информация о сервере {ctx.guild.name}",
                              color=0x8691c8)
        embed.set_thumbnail(url=ctx.guild.icon_url)
        embed.set_footer(text=f"ID: {ctx.guild.id}")
        embed.add_field(name="Участники",
                        value=f"<:usercolored:884068732954112010> Участников: **{online + idle + dnd + offline}**\n<:botlabelcolored:884068806387990629> Ботов: **{bots}**\n<:usergroupcolored:884068733100896318> Всего: **{online + idle + dnd + offline + bots}**",
                        inline=True)
        embed.add_field(name="Статусы",
                        value=f"<:online:883791849607790602> Онлайн: **{online}**\n<:idle:883791849796546560> Неактивен: **{idle}**\n<:dnd:883791849456816169> Не беспокоить: **{dnd}**\n<:offline:883791849318408203> Не в сети: **{offline}**",
                        inline=True)
        embed.add_field(name="Каналы и категории",
                        value=f"<:textcolored:884068732526280746> Текстовых: **{len(ctx.guild.text_channels)}**\n<:voicecolored:884068733658751026> Голосовых: **{len(ctx.guild.voice_channels)}**\n<:stagecolored:884068732874399774> Трибун: **{len(ctx.guild.stage_channels)}**\n<:categoriescolored:884150408614936637> Категорий: **{len(ctx.guild.categories)}**",
                        inline=True)
        embed.add_field(name="Владелец", value=ctx.guild.owner, inline=True)
        embed.add_field(name="Уровень проверки", value=verif_level.get(ctx.guild.verification_level, "Ошибка"), inline=True)
        embed.add_field(name="Дата создания", value=f"**`{ctx.guild.created_at.strftime('%d.%m.%Y, %H:%M:%S')}`**",
                        inline=True)
        await ctx.send(embed=embed)

    @cog_ext.cog_slash(name="user", description="Посмотреть информацию о пользователе")
    async def user(self, ctx: SlashContext, member: discord.Member = None):
        if member is None:
            member = ctx.author
        status = {
            discord.Status.online: "<:online:883791849607790602> Онлайн",
            discord.Status.idle: "<:idle:883791849796546560> Отошел",
            discord.Status.dnd: "<:dnd:883791849456816169> Не беспокоить",
            discord.Status.offline: "<:offline:883791849318408203> Не в сети",
        }
        embed = discord.Embed(
            title=f"Информация о {member.display_name}",
            description=self.cache.get_user(member.id).biography,
            color=member.top_role.color)
        embed.set_thumbnail(url=member.avatar_url)
        embed.set_author(name=member.display_name, icon_url=member.avatar_url)
        embed.set_footer(text=f"ID: {member.id}")
        embed.add_field(name="Имя пользователя", value=member.name)
        embed.add_field(name="Статус", value=status[member.status], inline=False)
        embed.add_field(name="Присоединился",
                        value=f"<t:{int(member.joined_at.timestamp())}:D> (<t:{int(member.joined_at.timestamp())}:R>)",
                        inline=True)
        embed.add_field(name="Создал аккаунт", value=f"<t:{int(member.created_at.timestamp())}:f>", inline=True)
        await ctx.send(embed=embed)

    @cog_ext.cog_slash(name="Bio", description="Установить биографию")
    async def bio(self, ctx, *, bio: str = None):
        if bio is None:
            self.db.execute("UPDATE users_data SET `biography` = default WHERE (`user_id` = %s);", (ctx.author.id,))
        else:
            self.db.execute("UPDATE users_data SET `biography` = %s WHERE (`user_id` = %s);", (bio, ctx.author.id,))
        await self.cache.cache_users(ctx.author.id)
        await self.send_success(ctx, "Ваша биография успешно изменена", hidden=True)

