#     Simple Bot - multifunctional bot
#     Copyright (C) 2021  Shandy developer agency
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import configparser
import datetime

import discord
from discord.ext import commands
from discord_slash import SlashCommand

import cache
import database
from fun import Fun
from mod import Mod
from music import Music
from settings import Settings
from utilities import Utilities
from voice import Voice

config = configparser.ConfigParser()
config.read("config.ini", encoding='utf-8')

db = database.Database(
    host=config["DB"]["server"],
    user=config["DB"]["login"],
    database=config["DB"]["database"],
    password=config["DB"]["password"],
    port=config["DB"].getint("port")
)

cache = cache.Cache()

bot = commands.Bot(command_prefix="/", intents=discord.Intents.all())
slash = SlashCommand(bot, sync_commands=True)

bot.add_cog(Mod(bot, cache, db, config))
bot.add_cog(Music(bot, cache, db, config))
bot.add_cog(Voice(bot, cache, db, config))
bot.add_cog(Settings(bot, cache, db, config))
bot.add_cog(Utilities(bot, cache, db, config))
bot.add_cog(Fun(bot, cache, db, config))


@bot.event
async def on_ready():
    await cache.cache_servers()
    # check servers
    counter = list()
    start = datetime.datetime.now()
    for server_id in cache.get_server().keys():
        if server_id in [guild.id for guild in bot.guilds]:
            continue
        else:
            counter.append(server_id)
    for server_id in counter:
        cache.remove_server(server_id)
        print(f"{server_id} has been removed from cache")
    end = datetime.datetime.now()
    print(f"Выполнена чистка неиспользуемых серверов. Было удалено {len(counter)} серверов за {end-start}")
    # end check
    await cache.cache_users()
    print(f"Авторизован под {bot.user}")
    print(f"Кэшированные сервера: {[server.id for server in cache.get_server().values()]}")


@bot.event
async def on_guild_join(self, guild):
    db.execute(f"INSERT INTO servers_data (`server_id`) VALUES (%s);", (guild.id,))
    await cache.cache_servers(guild.id)


@bot.event
async def on_guild_remove(self, guild):
    db.execute(f"DELETE FROM servers_data WHERE `server_id` = %s;", (guild.id,))
    cache.remove_server(guild.id)


bot.run(config["Config"]["token"])
