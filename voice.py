#     Simple Bot - multifunctional bot
#     Copyright (C) 2021  Shandy developer agency
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import datetime

import discord
from discord.ext import commands
from discord_slash import cog_ext

import cache
from basic_module import BasicModule


class Voice(BasicModule):
    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        guild_voice_id = self.cache.get_server(member.guild.id).voice_channel
        if before.channel is not None and before.channel.id in self.cache.get_temp_channel():  # Check the validity of the channel that the user has entered and find it in a list of channels
            if member.id == self.cache.get_temp_channel(before.channel.id).owner.id and after.channel != before.channel:  # Set None instead of the owner when he exits the channel
                self.cache.get_temp_channel(before.channel.id).change_owner(None)
            if not before.channel.members:  # Deleting a channel if there are no members left in it
                await before.channel.delete()
                self.cache.remove_temp_channel(before.channel.id)
            else:  # Reset permissions to the channel from the old owner
                await before.channel.set_permissions(member, manage_channels=None, move_members=None)
        if after.channel == before.channel:  # If the user did not switch to another channel, but something else has changed
            return
        elif member.voice is None:  # If the user is not in the channel
            return
        elif member.voice.channel.id == guild_voice_id:  # If the user has entered a special channel to create temporary channel
            channel = self.bot.get_channel(guild_voice_id)
            category = channel.category
            user_data = self.cache.get_user(member.id)
            # Bot creates a channel, moves the user there and add a channel to the cache
            if user_data is None:  # If the user is not in the database yet
                self.db.execute("INSERT INTO users_data (`user_id`, `channel_name`) VALUES (%s, %s);", (member.id, f"Канал {member.display_name}",))
                created_channel = await category.create_voice_channel(
                    name=f"Канал {member.display_name}",
                    user_limit=None
                )
            else:
                created_channel = await category.create_voice_channel(name=user_data.channel_name, user_limit=user_data.channel_limit, bitrate=(user_data.channel_bitrate if user_data.channel_bitrate <= int(member.guild.bitrate_limit) else int(member.guild.bitrate_limit)))
            await member.move_to(channel=created_channel)
            await created_channel.set_permissions(member, manage_channels=True, move_members=True)
            self.cache.add_temp_channel(
                cache.TemporaryChannel(
                    channel=created_channel,
                    creation_time=datetime.datetime.now(),
                    owner=member
                )
            )

    @commands.group(invoke_without_command=True, aliases=["v"])
    async def voice(self, ctx):
        embed = discord.Embed(title="Неверное использование!", colour=0x8691c8)
        embed.add_field(name=f"{ctx.prefix}voice name", value="Установить название канала")
        embed.add_field(name=f"{ctx.prefix}voice limit", value="Установить лимит пользователей в канале")
        embed.add_field(name=f"{ctx.prefix}voice claim", value="Стать владельцем канала")
        embed.add_field(name=f"{ctx.prefix}voice bitrate", value="Установить битрейт в канале")
        await ctx.send(embed=embed)

    @cog_ext.cog_subcommand(base="voice", name="name", description="Изменить название временного канала")
    async def name(self, ctx, *, channel_name: str):
        if ctx.author.voice is None:
            await self.send_error(ctx, "Вы не в канале")
        elif len(channel_name) > 100:
            await self.send_error(ctx, "Имя канала больше 100 символов")
        elif not self.cache.get_temp_channel():
            await self.send_error(ctx, "Вы находитесь не в персональном канале\nНет каналов в списке зарезервированных")
        elif self.cache.get_temp_channel(ctx.author.voice.channel.id).owner is None:
            await self.send_error(ctx, "Вы не являетесь создателем этого канала")
        else:
            if ctx.author.voice.channel.id in self.cache.get_temp_channel():
                if ctx.author.id == self.cache.get_temp_channel(ctx.author.voice.channel.id).owner.id:
                    self.cache.get_user(ctx.author.id).change_channel_name(channel_name)
                    await ctx.author.voice.channel.edit(name=channel_name)
                    await self.send_success(ctx, "Название успешно изменено")
                else:
                    await self.send_error(ctx, "Вы не являетесь создателем этого канала")
            else:
                await self.send_error(ctx, "Вы находитесь не в персональном канале")

    @name.error
    async def name_error(self, ctx, error):
        if isinstance(error, commands.CommandOnCooldown):
            time_s = round(error.retry_after) % 60
            await ctx.send(f'Эй! {ctx.author.mention}, подожди еще {time_s} секунд', delete_after=10)

    @cog_ext.cog_subcommand(base="voice", name="limit", description="Изменить лимит пользователей временного канала")
    async def limit(self, ctx, channel_limit: int):
        if ctx.author.voice is None:
            await self.send_error(ctx, "Вы не в канале")
        elif channel_limit <= -1 or channel_limit > 99:
            await self.send_error(ctx, "Вы указали неверный лимит\nОт `0` до `99`")
        elif not self.cache.get_temp_channel():
            await self.send_error(ctx, "Вы находитесь не в персональном канале\nНет каналов в списке зарезервированных")
        else:
            if ctx.author.voice.channel.id in self.cache.get_temp_channel():
                if ctx.author.id == self.cache.get_temp_channel(ctx.author.voice.channel.id).owner.id:
                    self.cache.get_user(ctx.author.id).change_channel_limit(channel_limit)
                    await ctx.author.voice.channel.edit(user_limit=channel_limit)
                    await self.send_success(ctx, "Лимит пользователей успешно изменён")
                else:
                    await self.send_error(ctx, "Вы не являетесь создателем этого канала")
            else:
                await self.send_error(ctx, "Вы не находитесь в персональном канале")

    @cog_ext.cog_subcommand(base="voice", name="claim", description="Присвоить временный канал себе")
    async def claim(self, ctx):
        print(self.cache.get_temp_channel())
        print(self.cache.get_temp_channel(ctx.author.voice.channel.id).owner)
        if ctx.author.voice is None:
            await self.send_error(ctx, "Вы не в канале")
        elif ctx.author.voice.channel.id not in self.cache.get_temp_channel():
            await self.send_error(ctx, "Вы не находитесь в персональном канале")
        elif self.cache.get_temp_channel(ctx.author.voice.channel.id).owner is not None and ctx.author.id == self.cache.get_temp_channel(ctx.author.voice.channel.id).owner.id:
            await self.send_error(ctx, "Вы уже являетесь владельцем этого канала")
        elif self.cache.get_temp_channel(ctx.author.voice.channel.id).owner is not None:
            await self.send_error(ctx, "Владелец канала находится в канале!")
        else:
            self.cache.get_temp_channel(ctx.author.voice.channel.id).change_owner(ctx.author)
            await ctx.author.voice.channel.set_permissions(ctx.author, manage_channels=True, move_members=True)
            await self.send_success(ctx, "Канал теперь ваш")

    @cog_ext.cog_subcommand(base="voice", name="bitrate", description="Изменить битрейт временного канала")
    async def bitrate(self, ctx, bitrate_int: int = 64):
        if ctx.author.voice is None:
            await self.send_error(ctx, "Вы не в канале")
        elif bitrate_int <= 7 or bitrate_int > int(ctx.guild.bitrate_limit) / 1000:
            await self.send_error(ctx, f"Вы указали неверный лимит\nОт `8` до `{int(ctx.guild.bitrate_limit) / 1000}`")
        elif not self.cache.get_temp_channel():
            await self.send_error(ctx, "Вы находитесь не в персональном канале\nНет каналов в списке зарезервированных")
        else:
            if ctx.author.voice.channel.id in self.cache.get_temp_channel():
                if ctx.author.id == self.cache.get_temp_channel(ctx.author.voice.channel.id):
                    self.cache.get_user(ctx.author.id).change_channel_bitrate(bitrate_int*1000)
                    await ctx.author.voice.channel.edit(bitrate=bitrate_int * 1000)
                    await self.send_success(ctx, f"Битрейт канала установлен на значение **{bitrate_int}**")

    @bitrate.error
    async def bitrate_error(self, ctx, error):
        if isinstance(error, discord.ext.commands.errors.BadArgument):
            await ctx.message.reply(f'Вы должны указать число!\nПример: `{ctx.prefix}voice bitrate 96`', delete_after=10)
