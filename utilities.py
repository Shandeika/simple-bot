#     Simple Bot - multifunctional bot
#     Copyright (C) 2021  Shandy developer agency
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import datetime
import discord
from discord.ext import commands

from basic_module import BasicModule


class Utilities(BasicModule):
    @commands.command()
    async def info(self, ctx):
        embed = discord.Embed(title=f"Информация о {self.bot.user.name}",
                              description="Простой бот, объединяющий в себе кучу функций.",
                              colour=0x7c93ce)
        embed.set_author(name="Shandy", url="https://github.com/Shandeika", icon_url="https://cdn.discordapp.com/avatars/335464992079872000/1bcab9849afc227c8b5b2ce063757a59.webp")
        embed.set_footer(text=f'Copyright © 2019–{datetime.date.today().strftime("%Y")} Shandy developer agency All Rights Reserved. © {datetime.date.today().strftime("%Y")}')
        embed.set_thumbnail(url="https://cdn.discordapp.com/attachments/846876879772188682/885877518060625960/simple_bot_2x1.png")
        embed.set_image(url="https://media.discordapp.net/attachments/846876879772188682/885877515288203315/simple_bot_4x1.png")
        await ctx.send(embed=embed)

    @commands.command()
    async def ping(self, ctx):
        if round(self.bot.latency * 1000) <= 50:
            embed = discord.Embed(title="PING",
                                  description=f":ping_pong: Пинг **{round(self.bot.latency * 1000)}** миллисекунд!",
                                  color=0x44ff44)
        elif round(self.bot.latency * 1000) <= 100:
            embed = discord.Embed(title="PING",
                                  description=f":ping_pong: Пинг **{round(self.bot.latency * 1000)}** миллисекунд!",
                                  color=0xffd000)
        elif round(self.bot.latency * 1000) <= 200:
            embed = discord.Embed(title="PING",
                                  description=f":ping_pong: Пинг **{round(self.bot.latency * 1000)}** миллисекунд!",
                                  color=0xff6600)
        else:
            embed = discord.Embed(title="PING",
                                  description=f":ping_pong: Пинг **{round(self.bot.latency * 1000)}** миллисекунд!",
                                  color=0x990000)
        await ctx.send(embed=embed)
