#     Simple Bot - multifunctional bot
#     Copyright (C) 2021  Shandy developer agency
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
from configparser import ConfigParser

import discord
from discord.ext import commands
from discord_slash import SlashContext

from cache import Cache
from database import Database


class BasicModule(commands.Cog):
    def __init__(self, bot: commands.Bot, cache: Cache, db: Database, config: ConfigParser):
        self.bot = bot
        self.cache = cache
        self.db = db
        self.config = config

    @staticmethod
    async def send_success(ctx, message, hidden=False):
        embed = discord.Embed(title=f"✅ {message}", colour=0x77b255)
        if isinstance(ctx, SlashContext):
            await ctx.send(embed=embed, hidden=hidden)
        else:
            await ctx.send(embed=embed)

    @staticmethod
    async def send_error(ctx, message, hidden=False):
        embed = discord.Embed(title=f"❌ {message}", colour=0xdd2e44)
        if isinstance(ctx, SlashContext):
            await ctx.send(embed=embed, hidden=hidden)
        else:
            await ctx.send(embed=embed)
