#     Simple Bot - multifunctional bot
#     Copyright (C) 2021  Shandy developer agency
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
import configparser
import datetime
from typing import Union, List, Dict

import discord

import database

config = configparser.ConfigParser()
config.read("config.ini", encoding='utf-8')

db = database.Database(
    host=config["DB"]["server"],
    user=config["DB"]["login"],
    database=config["DB"]["database"],
    password=config["DB"]["password"],
    port=config["DB"].getint("port")
)


class TemporaryChannel:
    def __init__(self, channel: discord.VoiceChannel, creation_time: datetime.datetime, owner: discord.Member):
        self.channel = channel
        self.creation_time = creation_time
        self.owner = owner
        self.owners_history = list()

        self.owners_history.append(owner)  # Add user to history

    def change_owner(self, new_owner: discord.Member) -> List[discord.Member]:
        self.owner = new_owner
        self.owners_history.append(new_owner)
        return self.owners_history

    def update_channel(self, new_channel: discord.VoiceChannel) -> None:
        self.channel = new_channel


class Server:
    def __init__(self, id: int, voice_channel: int):
        self.id = id
        self.voice_channel = voice_channel

    def change_voice_channel(self, new_voice_channel: int):
        self.voice_channel = new_voice_channel
        db.execute("UPDATE servers_data SET `voice_channel` = %s WHERE (`server_id` = %s);", (new_voice_channel, self.id,))


class User:
    def __init__(self, id: int, channel_name: str, channel_limit: int, channel_bitrate: int, biography: str):
        self.id = id
        self.channel_name = channel_name
        self.channel_limit = channel_limit
        self.channel_bitrate = channel_bitrate
        self.biography = biography

    def change_channel_name(self, channel_name: str) -> None:
        self.channel_name = channel_name
        db.execute("UPDATE users_data SET `channel_name` = %s WHERE (`user_id` = %s);", (channel_name, self.id,))

    def change_channel_limit(self, channel_limit: int) -> None:
        self.channel_limit = channel_limit
        db.execute("UPDATE users_data SET `channel_limit` = %s WHERE (`user_id` = %s);", (channel_limit, self.id,))

    def change_channel_bitrate(self, channel_bitrate: int) -> None:
        self.channel_bitrate = channel_bitrate
        db.execute("UPDATE users_data SET `channel_bitrate` = %s WHERE (`user_id` = %s);", (channel_bitrate, self.id,))

    def change_biography(self, biography) -> None:
        self.biography = biography
        db.execute("UPDATE users_data SET `biography` = %s WHERE (`user_id` = %s);", (biography, self.id,))


class Cache:
    def __init__(self):
        self.servers = dict()
        self.users = dict()
        self.temp_channels = dict()

    async def cache_servers(self, id: int = None) -> None:
        if id is None:
            self.servers = dict()
            rows = db.fetch_all("SELECT * FROM servers_data;")
            for row in rows:
                self.servers[row.get("server_id")] = (Server(
                    row.get("server_id"),
                    row.get("voice_channel")
                ))
        else:
            data = db.fetch_one("SELECT * FROM servers_data WHERE (`server_id` = %s);", (id,))
            if data is None:
                self.add_server(id)
            else:
                self.get_server(id).voice_channel = data["voice_channel"]
        print('Выполнено кэширование серверов')

    def get_server(self, id: int = None) -> Union[Server, Dict[int, Server]]:
        if id is None:
            return self.servers
        elif self.servers.get(id) is None:
            return self.add_server(id)
        return self.servers.get(id)

    def add_server(self, server_id: int) -> Server:
        db.execute("INSERT INTO servers_data (`server_id`) VALUES (%s);", (server_id,))
        data = db.fetch_one("SELECT * FROM servers_data WHERE (`server_id` = %s)", (server_id,))
        self.servers[server_id] = Server(data["server_id"], data["voice_channel"])

    def remove_server(self, server_id: int) -> None:
        self.servers.pop(server_id)
        db.execute("DELETE FROM servers_data WHERE (`server_id` = %s);", (server_id,))

    async def cache_users(self, id: int = None) -> None:
        if id is None:
            self.users = dict()
            rows = db.fetch_all("SELECT * FROM users_data;")
            for row in rows:
                self.users[row.get("user_id")] = User(
                    row.get("user_id"),
                    row.get("channel_name"),
                    row.get("channel_limit"),
                    row.get("channel_bitrate"),
                    row.get("biography"),
                )
        elif self.get_user(id) is None:
            row = db.fetch_one("SELECT * FROM users_data WHERE (`user_id` = %s);", (id,))
            self.users[id] = User(
                    row.get("user_id"),
                    row.get("channel_name"),
                    row.get("channel_limit"),
                    row.get("channel_bitrate"),
                    row.get("biography"),
                )
        else:
            row = db.fetch_one("SELECT * FROM users_data WHERE (`user_id` = %s);", (id,))
            user = self.get_user(id)
            user.channel_name = row.get("channel_name")
            user.channel_limit = row.get("channel_limit")
            user.channel_bitrate = row.get("channel_bitrate")
            user.biography = row.get("biography")
        print("Выполнено кэширование пользователей")

    def get_user(self, id: int = None) -> Union[User, List[User]]:
        if id is None:
            return self.users
        return self.users.get(id)

    def add_temp_channel(self, temp_channel: TemporaryChannel) -> None:
        self.temp_channels[temp_channel.channel.id] = temp_channel

    def remove_temp_channel(self, channel_id: int) -> None:
        self.temp_channels.pop(channel_id)

    def get_temp_channel(self, channel_id: int = None) -> Union[TemporaryChannel, Dict[int, TemporaryChannel], None]:
        if channel_id is None:
            return self.temp_channels
        return self.temp_channels.get(channel_id)
